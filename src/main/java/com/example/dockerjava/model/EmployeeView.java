package com.example.dockerjava.model;

import lombok.Data;

@Data
public class EmployeeView {

    int id;
    String name;
    String gender;
    String role;
    double salary;
    String detail;

    public EmployeeView(Employee employee) {
        this.id = employee.getId();
        this.name = employee.getName();
        this.gender = employee.getGender();
        this.role = employee.getRole();
        this.salary = employee.getSalary();

        if(employee.getDetail() == null){
            this.detail = "";
        } else {
            this.detail = employee.getDetail();
        }
    }

}
