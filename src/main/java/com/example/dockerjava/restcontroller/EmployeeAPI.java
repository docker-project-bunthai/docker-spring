package com.example.dockerjava.restcontroller;

import com.example.dockerjava.model.Employee;
import com.example.dockerjava.model.EmployeeView;
import com.example.dockerjava.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.persistence.Column;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController("/v1/employees")
public class EmployeeAPI {
    @Autowired
    EmployeeService employeeService;

    @PostMapping
    public EmployeeView saveEmployee(@RequestBody Employee employee) {
        return new EmployeeView(employeeService.insertEmployee(employee).get());
    }

    @PatchMapping
    public EmployeeView editEmployee(@RequestBody Employee employee) {
        return new EmployeeView(employeeService.insertEmployee(employee).get());
    }

    @GetMapping("{id}")
    public EmployeeView getEmployee(@PathVariable("id") int id) {
        return new EmployeeView(employeeService.findEmployeeById(id).get());
    }

    @GetMapping
    public List<EmployeeView> getEmployees() {
        List<EmployeeView> employeeViews = new ArrayList<>();
        employeeService.findAllEmployee().forEach(e -> {
         employeeViews.add(new EmployeeView(e));
        });
         return employeeViews;
    }

    @RequestMapping(value = "/v1/employees/{id}", method = RequestMethod.DELETE)
//    @DeleteMapping("{id}")
    public EmployeeView deleteEmployee(@PathVariable("id") int id) {
        return new EmployeeView(employeeService.deleteEmployee(id).get());
    }

}
