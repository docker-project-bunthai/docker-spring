package com.example.dockerjava.service;

import com.example.dockerjava.model.Employee;

import java.util.List;
import java.util.Optional;

public interface EmployeeService {
    public Optional<Employee> insertEmployee(Employee employee);
    public Optional<Employee> findEmployeeById(int id);
    public List<Employee> findAllEmployee();
    public Optional<Employee> deleteEmployee(int id);
}
