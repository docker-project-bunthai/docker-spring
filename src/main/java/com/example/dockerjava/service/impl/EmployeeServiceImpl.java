package com.example.dockerjava.service.impl;

import com.example.dockerjava.model.Employee;
import com.example.dockerjava.repository.EmployeeRepository;
import com.example.dockerjava.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class EmployeeServiceImpl implements EmployeeService {

    @Autowired
    private EmployeeRepository employeeRepository;

    @Override
    public Optional<Employee> insertEmployee(Employee employee) {
        return Optional.of(employeeRepository.save(employee));
    }

    @Override
    public Optional<Employee> findEmployeeById(int id) {
        return employeeRepository.findById(id);
    }

    @Override
    public List<Employee> findAllEmployee() {
        return employeeRepository.findAll();
    }

    @Override
    public Optional<Employee> deleteEmployee(int id) {
        Optional<Employee> employee = findEmployeeById(id);
        employeeRepository.deleteById(id);
        return employee;
    }


}
