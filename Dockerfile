FROM openjdk:8-jre
MAINTAINER BunthaiDeng:dengbunthai@gmail.com

WORKDIR project/
COPY . .
WORKDIR target/
CMD ["java", "-jar", "docker-java-0.0.1-SNAPSHOT.jar"]


